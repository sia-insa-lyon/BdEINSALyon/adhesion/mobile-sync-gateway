from django.urls import re_path, path

from . import consumers

websocket_urlpatterns = [
    re_path(r'^admin/(?P<token>.{30})$', consumers.CompanionSyncComputer),
    re_path(r'^mobile/(?P<token>.{30})$', consumers.CompanionSyncHandset),
    path('chat/', consumers.Chat),
]
