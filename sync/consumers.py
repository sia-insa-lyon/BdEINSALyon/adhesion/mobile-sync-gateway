from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class CompanionSyncComputer(WebsocketConsumer):
    def connect(self):
        self.id = self.scope["url_route"]["kwargs"][
            "token"]  # plus tard on envoie le token à adhésion pour récupérer le nom d'utilisateur
        async_to_sync(self.channel_layer.group_add)(self.id, self.channel_name)

        self.accept()  # on continue
        # self.send("salut")

    def send_computer(self, event):
        print("envoi de {}".format(event['data']))
        self.send(text_data=event['data'])

    def computer_to_handset(self, event):
        pass

    def receive(self, text_data=None, bytes_data=None):
        print("rcv computer : {}".format(text_data))
        async_to_sync(self.channel_layer.group_send)(self.id, {'type': 'computer_to_handset', 'data': text_data})

    def disconnect(self, code):
        async_to_sync(self.channel_layer.group_discard)(self.id, self.channel_name)


class CompanionSyncHandset(WebsocketConsumer):
    def connect(self):
        self.id = self.scope["url_route"]["kwargs"][
            "token"]  # plus tard on envoie le token à adhésion pour récupérer le nom d'utilisateur
        async_to_sync(self.channel_layer.group_add)(self.id, self.channel_name)
        self.accept()  # on continue
        self.send("salut a un tel")

    def receive(self, text_data=None, bytes_data=None):
        print("rcv handset: {}".format(text_data))
        async_to_sync(self.channel_layer.group_send)(self.id, {'type': 'send_computer', 'data': text_data})
        if text_data == '{"status":"disconnected","type":"status_message"}':
            self.disconnect(0)

    def computer_to_handset(self, event):
        print(f"forwading {event} to handset")
        self.send(text_data=event['data'])

    def send_computer(self, event):
        pass

    def disconnect(self, code):
        async_to_sync(self.channel_layer.group_discard)(self.id, self.channel_name)


class Chat(WebsocketConsumer):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)("chat", self.channel_name)
        self.accept()

    def receive(self, text_data=None, bytes_data=None):
        async_to_sync(self.channel_layer.group_send)("chat", {'type': 'broadcast', 'data': text_data})

    def broadcast(self, event):
        self.send(text_data=event['data'])
