import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.shortcuts import render
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from requests import Request
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['POST'])
def test(request: Request):
    print("rcv")
    print(request.data)

    return Response(request.data)


@api_view(['POST'])
def receive_from_handset(request: Request, token):
    #token = request.params.get('token')
    async_to_sync(get_channel_layer().group_send)(token, {'type': 'send_computer', 'data': json.dumps(request.data)})
   # print(request.data)

    return Response(request.data)
