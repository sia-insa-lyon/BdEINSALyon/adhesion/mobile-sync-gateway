FROM python:3.6

EXPOSE 8000
WORKDIR /app
COPY . /app
RUN pip install -r /app/requirements.txt
RUN chmod +x run.sh
CMD /app/run.sh
